#!/bin/bash

cd /var/www/content/online-marketing-assistant-bot/web/

. wait_for_mariadb.sh
. wait_for_mysql.sh

/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS 'omabot_web'@'%' IDENTIFIED BY '123123123';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS omabot_web CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON omabot_web.* TO 'omabot_web'@'%';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'

/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS 'omabot_web'@'%' IDENTIFIED BY '123123123';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS omabot_web CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON omabot_web.* TO 'omabot_web'@'%';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'

#ssh-keyscan -H gitlab.com >> /root/.ssh/known_hosts
#php -d memory_limi=-1 -d max_execution_time=0 ../../composer.phar install --no-interaction

